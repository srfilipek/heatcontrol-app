/*********************************************************************************
 Copyright (C) 2016 by Stefan Filipek

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*********************************************************************************/

#include <inttypes.h>
#include <math.h>

#include <array>
#include <list>

#include "application.h"
#include "system.h"
#include "w1.h"
#include "dstherm.h"
#include "zone.h"
#include "control.h"

namespace Control {

int polls;          // Just FYI, number of times the zones have been polled
int last_poll;      // Last time data was polled from the zones

std::array<Zone, 3> zone_list{
    // Zone, w1_pin, set_pin, reset_pin
    Zone(0, D2, A5, A4),
    Zone(1, D1, A3, A2),
    Zone(2, D0, A1, A0),
};

/**
 * Set the mode for the given zone
 *
 * @param info: Format: "zone_index:mode"
 *
 * Where zone_index is a number in [0, len(zone_list)) and
 * mode is in {"heat", "off"}
 */
int setMode(String info) {
    size_t zone = info.charAt(0) - '0';

    if (zone >= zone_list.size()) {
        // Invalid zone
        return -1;
    }

    if (info.charAt(1) != ':') {
        // Malformed
        return -1;
    }

    // Get the mode after the colon
    String mode = info.substring(2);

    if (mode == "heat") {
        zone_list[zone].setMode(Zone::MODE_HEAT);
    } else if (mode == "off") {
        zone_list[zone].setMode(Zone::MODE_OFF);
    } else {
        return -1;
    }

    zone_list[zone].publishStatusString();

    return 0;
}


/**
 * Set the temp for the given zone
 *
 * @param info: Format: "zone_index:temperature"
 *
 * Where zone_index is a number in [0, len(zone_list)) and
 * temperature (Farenheit) is a float in [TEMP_MIN, TEMP_MAX].
 */
int setDesired(String info) {
    size_t zone = info.charAt(0) - '0';

    if (zone >= zone_list.size()) {
        // Invalid zone
        return -1;
    }

    if (info.charAt(1) != ':') {
        // Malformed
        return -1;
    }

    // Get the temp after the colon
    float temp = info.substring(2).toFloat();

    if (temp < Zone::TEMP_MIN || temp > Zone::TEMP_MAX) {
        return -1;
    }

    // Finally...
    zone_list[zone].setDesired(temp);

    zone_list[zone].publishStatusString();

    return 0;
}


/**
 * Poll the temp of all zones in our system
 */
void process_all_zones(void)
{
    // Iterate over every zone that we have
    for(auto & zone : zone_list) {
		zone.process(true);
    }

    int time = Time.now();
    last_poll = time;

    // Increase poll count, with rollover to 0 (instead of negative)
    polls += 1;
    if(polls < 0) polls = 0;
}

void setup(void)
{
    // Set the control lines
    for(auto & zone : zone_list) {
        zone.setup();   
    }

    Particle.function("setMode", setMode);
    Particle.function("setDesired", setDesired);
   
    Particle.variable("ctrl_polls", polls);
    Particle.variable("ctrl_time",  last_poll);
}

};

