/*********************************************************************************
 Copyright (C) 2016 by Stefan Filipek

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*********************************************************************************/

#include <array>
#include <list>

// Particle stuff
#include <application.h>

// Our own headers
#include "system.h"
#include "sense.h"
#include "control.h"

SYSTEM_MODE(SYS_MODE);
SYSTEM_THREAD(SYS_THREAD);

STARTUP(Serial.begin(115200));
STARTUP(System.enableFeature(FEATURE_RETAINED_MEMORY));

SerialLogHandler logHandler;
ApplicationWatchdog wd(WD_TIMEOUT, System.reset);


/**
 * Send a monotically increasing heartbeat value to test the network connectivity.
 */
void send_heartbeat() {
    static unsigned long hb = 0;
    if (Particle.connected()) {
        Particle.publish("heartbeat", String(hb++), TTL_HB, PRIVATE);
    }
}


/**
 * Synchronize our clock with the network.
 */
void sync_time() {
    Particle.syncTime();
}


/**
 * Generic function to rate-limit a standard function call.
 *
 * This takes care of keeping track of the last time a function was called.
 */
template<unsigned long ACTION_DELAY_MS, void (*function)(), unsigned long START_OFFSET=0>
void rate_limited_call() {
    static unsigned long last_action = START_OFFSET;
    unsigned long now = millis();
    if((now - last_action) < ACTION_DELAY_MS) return;

    last_action = now;
    function();
}


void setup()
{
    Log.info("Setting up heat monitor");

    Sense::setup();
    Control::setup();

    Log.info("Setup complete");
}


void loop()
{
    static const unsigned long HALF_DAY_MS  = 12*60*60*1000;
    static const unsigned long MINUTE_MS    = 60*1000;
    static const unsigned long CONTROL_TIME = 5*1000;
    static const unsigned long SENSE_TIME   = 10;

    // Ensure the heartbeat is sent right away by offsetting its start time
    rate_limited_call<MINUTE_MS,    send_heartbeat, -MINUTE_MS>();
    rate_limited_call<SENSE_TIME,   Sense::process_all_zones>();
    rate_limited_call<CONTROL_TIME, Control::process_all_zones>();
    rate_limited_call<HALF_DAY_MS,  sync_time>();
}

