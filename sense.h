/*********************************************************************************
 Copyright (C) 2016 by Stefan Filipek

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*********************************************************************************/

#ifndef _SENSE_H_
#define _SEHSE_H_

namespace Sense {

/**
 * Hysteresis values for polling the input lines
 * We are polling a 60Hz signal when the zone demand is on. Fortunately, the
 * probability of seeing "high" on the line during this time is low, so we can
 * do a simple average over time, hysteresis, or other debounce technique.
 *
 * Let's use hysteresis...
 *
 * A low count will mean that the zone is off. A high count means the zone
 * demand is on. Use a simple threshold detector, evenly spaced.
 */
const int HYST_MIN          = 0;
const int HYST_MAX          = 500;
const int HYST_DEFAULT      = HYST_MAX/2;
const int HYST_THRESH_ON    = 400; // Turn on when count climbs to this
const int HYST_THRESH_OFF   = 100; // Turn off when count falls to this

/**
 * Truthiness values of a zone's demand
 */
enum Demand {
    Unknown,
    True,
    False 
};

/**
 * Zone information/configuration
 */
struct ZoneInfo {
    int id;
    uint16_t pin;
    int count;
    int demand;
};


/**
 * Zone events -- when a zone turned on or off
 */
struct ZoneEvent {
    ZoneInfo info;
    int time;
    int demand;
};

const size_t MAX_NUM_EVENTS = 20;


const char * const ZONE_JSON_STRING = "{\"id\":%d,\"t\":%d,\"on\":%d}";


/**
 * Setup any variables and perform any other configuration necessary
 */
void setup(void);

/**
 * Poll the status of all zones in our system.
 *
 * This will publish events and update variables as necessary.
 */
void process_all_zones(void);

};

#endif
