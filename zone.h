/*********************************************************************************
 Copyright (C) 2016 by Stefan Filipek

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*********************************************************************************/

#ifndef _zone_h_defined_
#define _zone_h_defined_

#include <math.h>

#include "w1.h"
#include "dstherm.h"


/**
 * Zone information/configuration
 */
class Zone {
public:
    /**
     * Temperatures are in millidegrees F
     */
    static constexpr float TEMP_MIN          = 50.0;
    static constexpr float TEMP_MAX          = 80.0;
    static constexpr float TEMP_DEFAULT      = NAN;
    static constexpr float TEMP_SET_DEFAULT  = 60.0;

    // Ensure we publish status within a certain interval
    static constexpr unsigned long MAX_STATUS_DELTA_MS = 1000 * 60; // Every 1 minute (in ms)
    
    // Hysteresis is performed by a deadband
    static constexpr float BAND_PLUS         = 0.50; // Turn heat off at desired plus this value
    static constexpr float BAND_MINUS        = 0.50; // Turn heat on at desired minus this value
    static constexpr uint8_t SETTINGS_VER    = 1;
   

    enum Mode {
        MODE_OFF,
        MODE_HEAT,
    };
    
    enum Health {
        HEALTH_UNKNOWN,
        HEALTH_ERROR,
        HEALTH_NORMAL,
    };

    enum HeatState {
        HEAT_UNSET,
        HEAT_ON,
        HEAT_OFF,
    };
    
    enum ConversionState {
        CONVERSION_IDLE,
        CONVERSION_STARTED,
    };

    // This information gets stored to EEPROM 
#pragma pack(push, 1)
    struct Settings {
        uint8_t     version;
    	float       temp;
    	Mode        mode;
        uint16_t    crc; // Ensure data vaildity in EEPROM
    };
#pragma pack(pop)

    constexpr Zone(float id, int pin_w1, int pin_set, int pin_reset) :
        id(id),
        pin_w1(pin_w1),
        pin_set(pin_set),
        pin_reset(pin_reset),
        w1_bus(pin_w1),
        w1_addr(W1::ADDRESS_NULL),
        temp_actual(TEMP_DEFAULT),
        health(HEALTH_UNKNOWN),
        heat_state(HEAT_UNSET),
        conv_state(CONVERSION_IDLE),
        settings(Settings{SETTINGS_VER, TEMP_SET_DEFAULT, MODE_OFF, 0}),
        bus_errors(0),
        total_bus_errors(0),
        status_string(),
        last_status_string(),
        last_status_publish(0)
    {
        // Nothing...
    }

    void setup();

    /**
     * Examine the state of the system and perform any necessary updates
     *
     * @param update_temp: Check/update the temperature reading first
     */
    void process(bool update_temp);

    void setDesired(float temp);

    void setMode(Mode mode);

    float getActual() const;

    float getDesired() const;

    Mode getMode() const;

    void publishStatusString();


private:
    static const size_t JSON_STATUS_STRING_SIZE = 100;
    static const char JSON_STATUS_STRING[];
    static const size_t BUS_ERROR_LIMIT = 5; // In a row, really

    void readTemp();

    void checkTemp();

    void setHeat(bool on);

    bool findSensor();

    void startConversion(DSTherm &therm);
    void checkConversion(DSTherm &therm);

    void errorState();
    void healthyState(const W1::Address &address);

    void loadSettings();
    void saveSettings();

    int id;                         // Zone ID
    int pin_w1;                     // 1-Wire data line
    int pin_set;                    // Demand relay on
    int pin_reset;                  // Demand relay off
    W1 w1_bus;                      // Persistent 1-Wire bus object
    W1::Address w1_addr;            // 1-Wire device address
    float temp_actual;              // Actual temperature reading (millidegrees)
    Health health;                  // General status of the zone
    HeatState heat_state;           // Status of the heat controls
    ConversionState conv_state;     // Temperature conversion status
	Settings settings;              // User settings
    size_t bus_errors;              // Bus error count (in a row)
    int    total_bus_errors;        // Total number of bus errors (int because of Particle)

    char status_string[JSON_STATUS_STRING_SIZE];
    char last_status_string[JSON_STATUS_STRING_SIZE];
    unsigned long last_status_publish;
};

#endif
