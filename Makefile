TARGET_TEST:=test-photon
TARGET_DEPLOY:=harpy
PROJECT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

.PHONY: all load clean

all: firmware.bin

clean:
	rm firmware.bin

load: firmware.bin
	particle flash ${TARGET_TEST} firmware.bin
	
deploy: firmware.bin
	particle flash ${TARGET_DEPLOY} firmware.bin
	
firmware.bin: *.cpp *.h
	docker run -t --rm \
		-v ${PROJECT_DIR}:/input \
		-v ${PROJECT_DIR}:/output \
		-e PLATFORM_ID=6 \
		particle/buildpack-particle-firmware:0.6.3 /bin/run

