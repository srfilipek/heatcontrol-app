/*********************************************************************************
 Copyright (C) 2016 by Stefan Filipek

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*********************************************************************************/

#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#define SYS_MODE AUTOMATIC
//#define SYS_THREAD DISABLED
//#define SYS_MODE SEMI_AUTOMATIC
#define SYS_THREAD ENABLED

// Particle variable configuration
static const int TTL_HB = 60;
static const int TTL_DEMAND = 120;
static const int TTL_CONTROL = 60;
static const size_t PARTICLE_VARIABLE_SIZE = 12;
static const size_t PARTICLE_STRING_SIZE = 622;
static const int WD_TIMEOUT = 10 * 1000; // 10 second timeout for loop execution

#endif
