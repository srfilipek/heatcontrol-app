/*********************************************************************************
 Copyright (C) 2016 by Stefan Filipek

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*********************************************************************************/

#include <inttypes.h>
#include <math.h>

#include <array>
#include <list>

#include "application.h"
#include "system.h"
#include "w1.h"
#include "dstherm.h"
#include "crc.h"
#include "zone.h"

#ifndef EEPROM_SETTINGS
retained std::array<Zone::Settings, 3> settings_list {
    Zone::Settings{Zone::SETTINGS_VER, Zone::TEMP_SET_DEFAULT, Zone::MODE_OFF, 0},
    Zone::Settings{Zone::SETTINGS_VER, Zone::TEMP_SET_DEFAULT, Zone::MODE_OFF, 0},
    Zone::Settings{Zone::SETTINGS_VER, Zone::TEMP_SET_DEFAULT, Zone::MODE_OFF, 0},
};
#endif

const char Zone::JSON_STATUS_STRING[] = "{\"id\":%d,\"act\":\"%.4f\",\"des\":\"%.4f\",\"heat\":\"%s\",\"mode\":\"%s\",\"health\":\"%s\"}";

constexpr uint16_t swap_16(uint16_t input) {
    return ((input & 0xFF00) >> 8) | ((input & 0x00FF) << 8);
}

/**
 * Pulse the set or reset line for a relay
 */
void pulseOn(int pin) {
    pinSetFast(pin);
    delay(15); // At least 10ms, by the docs
    pinResetFast(pin);
}


void Zone::errorState() {
    health = HEALTH_ERROR;
    w1_addr = W1::ADDRESS_NULL;
    temp_actual = NAN;
    bus_errors = 0;
}


void Zone::healthyState(const W1::Address &address) {
    health = HEALTH_NORMAL;
    w1_addr = address;
    bus_errors = 0;
}


/**
 * Searches for a temp sensor on the 1-wire bus
 */
bool Zone::findSensor() {
    // Perform a test on the 1-wire bus to ensure we have a proper pull-up
    pinMode(pin_w1, INPUT_PULLDOWN); // Weaker than our pull-up
    if (pinReadFast(pin_w1) == LOW) {
        errorState();
        return false;
    }
    pinMode(pin_w1, INPUT);

    // Check if there are any 1-wire devices
    if (!w1_bus.reset()) {
        errorState();
        return false;
    }

    Log.info("Zone %d: Found 1-wire device", id);

    W1::Address address;
    W1::Token   token = W1::EMPTY_TOKEN;
    while(w1_bus.search_rom(address, token)) {
        Log.info("  Address %08x%08x", (unsigned int)(address.raw >> 32), (unsigned int)address.raw);

        healthyState(address);
        return true;
    }
    return false;
}


/**
 * Initialize the zone
 */
void Zone::setup() {
    // On reset, pin output register defaults to LOW
    pinMode(pin_set, OUTPUT);
    pinMode(pin_reset, OUTPUT);

    // Search for and save away any 1-wire devices
    findSensor();

    // Create some variables for direct access to the zone's status
    char buf[PARTICLE_VARIABLE_SIZE + 1];

    snprintf(buf, PARTICLE_VARIABLE_SIZE, "z%d_control", id);
    Particle.variable(buf, status_string);

    snprintf(buf, PARTICLE_VARIABLE_SIZE, "z%d_errors", id);
    Particle.variable(buf, total_bus_errors);

    loadSettings();
}


void Zone::loadSettings() {
    // Get any info out of EEPROM
#ifdef EEPROM_SETTINGS
    Settings stored_settings;
    int address = EEPROM_ZONE_SETTINGS_BASE + (sizeof(Settings) * id);
    EEPROM.get(address, stored_settings);
#else
    Settings & stored_settings = settings_list[id];
#endif

    // Validate
    uint16_t crc = crc::crc16(
            &stored_settings,
            sizeof(Settings),
            crc::CRC_16_CCITT_INIT, crc::CRC_16_CCITT_POLY);

    if (settings.version != SETTINGS_VER || crc != 0) {
        settings = Settings{SETTINGS_VER, TEMP_SET_DEFAULT, MODE_OFF, 0};
        Log.warn("Invalid zone %d settings", id);
    } else {
        Log.info("Loaded zone %d settings", id);
        settings = stored_settings;
    }
}


void Zone::saveSettings() {
    // Calculate and store the CRC
    uint16_t crc = crc::crc16(
            &settings,
            sizeof(Settings) - sizeof(uint16_t),
            crc::CRC_16_CCITT_INIT, crc::CRC_16_CCITT_POLY);

    settings.crc = swap_16(crc);

#ifdef EEPROM_SETTINGS
    int address = EEPROM_ZONE_SETTINGS_BASE + (sizeof(Settings) * id);
    EEPROM.put(address, settings);
#else
    settings_list[id] = settings;
#endif
}


/**
 * Handle periodic maintenance
 */
void Zone::process(bool update_temp) {
    if (update_temp) {
        readTemp(); // Will update health state
    }

    switch(health) {
        case HEALTH_UNKNOWN:
            break;

        case HEALTH_ERROR:
            setHeat(false);
            break;

        case HEALTH_NORMAL:
            checkTemp();
            break;

        default:
            Log.error("Zone %d: Unknown health state %d", id, health);
            errorState();
            break;
    };

    publishStatusString();
}


void Zone::startConversion(DSTherm &therm) {
    if (therm.start_conversion() == false) {
        Log.error("Zone %d: Conversion failed", id);
        conv_state = CONVERSION_IDLE;
        bus_errors++;
        total_bus_errors++;
    } else {
        conv_state = CONVERSION_STARTED;
    }
}

void Zone::checkConversion(DSTherm &therm) {
    if (therm.conversion_done() == false) {
        // There should have been plenty of time
        bus_errors++;
        total_bus_errors++;
        return;
    }

    conv_state = CONVERSION_IDLE;

    float new_temp = therm.get_temp(true);
    if (isnan(new_temp) || new_temp > 184.0) { // 185 F is the startup value
        // Don't change the health state... just try again
        Log.warn("Zone %d: %f", id, new_temp);
        bus_errors++;
        total_bus_errors++;
    } else {
        // Good temp
        temp_actual = new_temp;
        Log.info("Zone %d: Temp reading of %f", id, temp_actual);
        bus_errors = 0;
    }

    // Kick off a new reading immediately
    startConversion(therm);
}


/**
 * Read the current temp of the given zone
 */
void Zone::readTemp() {

    // Make sure we have a sensor to read
    if (w1_addr.raw == W1::ADDRESS_NULL.raw && findSensor() == false) {
        conv_state = CONVERSION_IDLE;
        return;
    }

    DSTherm therm(w1_bus, w1_addr);

	// Start a conversion
    switch (conv_state) {
        case CONVERSION_IDLE:
            startConversion(therm);
            break;

        case CONVERSION_STARTED:
            checkConversion(therm);
            break;

        default:
            Log.error("Zone %d: Unknown conversion state", id);
            conv_state = CONVERSION_IDLE;
            break;
    };

    // If we encounter too many errors, give up and try again (will trigger findSensor)
    if (bus_errors >= BUS_ERROR_LIMIT) {
        Log.error("Zone %d: Too many bus errors", id);
        errorState();
    }
}


void Zone::checkTemp() {
    // Only support heat for now
    if (settings.mode != MODE_HEAT) {
        setHeat(false);
        return;
    }

    // Check the temps
    if (temp_actual >= (settings.temp + BAND_PLUS)) {
        setHeat(false);
    }

    if (temp_actual <= (settings.temp - BAND_MINUS)) {
        setHeat(true);
    }
}


/**
 * Enable or disable heat. This will only energize the relay on change.
 */
void Zone::setHeat(bool on) {
    if (on) {
        if (heat_state == HEAT_ON) {
            return;
        }

        Log.info("Zone %d: heat on", id);
        pulseOn(pin_set);
        heat_state = HEAT_ON;

    } else {
        if (heat_state == HEAT_OFF) {
            return;
        }

        Log.info("Zone %d: heat off", id);
        pulseOn(pin_reset);
        heat_state = HEAT_OFF;
    }
}


void Zone::setMode(Zone::Mode mode) {
    Log.info("Zone %d: Setting mode to %d", id, mode);

    settings.mode = mode;
    saveSettings();

    process(false);
}


Zone::Mode Zone::getMode() const {
    return settings.mode;
}


void Zone::setDesired(float temp) {
    Log.info("Zone %d: Setting temp to %f", id, temp);

    settings.temp = temp;
    saveSettings();

    process(false);
}


float Zone::getDesired() const {
    return settings.temp;
}


float Zone::getActual() const {
    return temp_actual;
}


const char* modeString(Zone::Mode mode) {
    switch (mode) {
        case Zone::MODE_OFF:
            return "off";
        case Zone::MODE_HEAT:
            return "heat";
        default:
            return "invalid";
    };
}

const char* healthString(Zone::Health health) {
    switch (health) {
        case Zone::HEALTH_UNKNOWN:
            return "unknown";
        case Zone::HEALTH_ERROR:
            return "error";
        case Zone::HEALTH_NORMAL:
            return "normal";
        default:
            return "invalid";
    };
}

const char* heatStateString(Zone::HeatState state) {
    switch (state) {
        case Zone::HEAT_UNSET:
            return "unset";
        case Zone::HEAT_OFF:
            return "off";
        case Zone::HEAT_ON:
            return "on";
        default:
            return "invalid";
    };
}


void Zone::publishStatusString() {
    snprintf(status_string, sizeof(status_string), JSON_STATUS_STRING,
            id, temp_actual, settings.temp, heatStateString(heat_state), modeString(settings.mode), healthString(health));

    unsigned long now = millis();

    // Only publish on change or we haven't published in a while
    if ((now - last_status_publish) > MAX_STATUS_DELTA_MS ||
            strncmp(last_status_string, status_string, JSON_STATUS_STRING_SIZE)) {
        if (Particle.connected()) {
            Particle.publish("zone_control", status_string, TTL_CONTROL, PRIVATE);
        }
        strncpy(last_status_string, status_string, JSON_STATUS_STRING_SIZE);
        last_status_publish = now;
    }

}

